def solution(total_lambs):
    # Your code here
    def generous_count(total):
        if total < 1:
            return 0
        n = 1
        pay_sum = 1
        pay_next = 2
        while (pay_sum + pay_next <= total):
            pay_sum += pay_next
            pay_next *= 2
            n += 1
        return n

    def stingy_count(total):
        if total < 1:
            return 0
        if total == 1:
            return 1
        n_2 = 1
        n_1 = 1
        pay_sum = 2
        pay_next = 2
        n = 2
        while (pay_sum + pay_next <= total):
            pay_sum += pay_next
            n_2 = n_1
            n_1 = pay_next
            pay_next = n_1 + n_2
            n += 1
        return n

    return stingy_count(total_lambs) - generous_count(total_lambs)

cases = [
 (143, 3),
 (10, 1),
]

for total, exp in cases:
 res = solution(total)
 print(total, res == exp, res, exp)
