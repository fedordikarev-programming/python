def solution(xs):
    # Your code here
    xs.sort()
    prod = long(1)
    i = 0
    l = len(xs)
    
    if l < 1:
        return 1
    if l == 1:
        return str(xs[0])
        """
        if xs[0] <= 0:
            return 1
        else:
            return xs[0]
        """
    took_some = False
    while (i+1 < l) and xs[i+1] < 0:
        prod *= (xs[i]*xs[i+1])
        i += 2
        took_some = True
    if i >= l:
        if took_some:
            return prod
        return xs[-1]
    have_zeroes = False
    while (i < l) and (xs[i] < 0):
        i += 1
    while (i < l) and (xs[i] == 0):
        i += 1
        have_zeroes = True
    have_positive = False
    while (i < l) and (xs[i] > 0):
        prod *= xs[i]
        i += 1
        have_positive = True
    if not have_positive:
        if have_zeroes:
            return '0'
        return str(xs[-1])
    return str(prod)

cases = [
	([2, 0, 2, 2, 0], '8'),
	([-2, -3, 4, -5], '60'),
	([-5, 2], '2'),
	([-5,], '-5'),
    ([-5, 0], '0'),
    ([-5, 0, 0, 0, 0, 0], '0'),
    ([0, 1, 2, 3], '6'),
    ([1, 2, 3], '6'),
    ([-5, -3, 2], '30'),
    ([-5, -3, 2, 2], '60'),
    ([-5, 1], '1'),
]

for xs, exp in cases:
    res = solution(xs)
    print(xs, res == exp, res, exp)
