def solution(map):
    # Your code here
    h = len(map)
    w = len(map[0])
    UNREACH = w*h+1000
    reach = [[[UNREACH, UNREACH] for i in range(w)] for j in range(h)]
    if map[0][0] != 0:
        return -1
    if map[h-1][w-1] != 0:
        return -1
    reach[0][0] = [0, 0]
    step = 0
    next_steps = [(0, 0)]
    while next_steps:
        (i, j) = next_steps.pop()
        for (d_i, d_j) in ((1, 0), (-1, 0), (0, 1), (0, -1)):
            if not (0 <= i+d_i < w):
                continue
            if not (0 <= j+d_j < h):
                continue
            nj = j + d_j
            ni = i + d_i
            new_step = False
            if map[nj][ni] == 0:
                new_reach = reach[j][i][0] + 1
                new_reach_wall = reach[j][i][1] + 1
                if new_reach < reach[nj][ni][0]:
                    reach[nj][ni][0] = new_reach
                    new_step = True
                if new_reach_wall < reach[nj][ni][1]:
                    reach[nj][ni][1] = new_reach_wall
                    new_step = True
            else:
                new_reach_wall = reach[j][i][0] + 1
                if new_reach_wall < reach[nj][ni][1]:
                    reach[nj][ni][1] = new_reach_wall
                    new_step = True
            if new_step:
                next_steps.append((ni, nj))

    return min(reach[h-1][w-1][0], reach[h-1][w-1][1])+1

cases = [
    ([[0, 1, 1, 0], [0, 0, 0, 1], [1, 1, 0, 0], [1, 1, 1, 0]], 7),
    ([[0, 0, 0, 0, 0, 0], [1, 1, 1, 1, 1, 0], [0, 0, 0, 0, 0, 0], [0, 1, 1, 1, 1, 1], [0, 1, 1, 1, 1, 1], [0, 0, 0, 0, 0, 0]], 11),
]

for map_c, exp in cases:
    ret = solution(map_c)
    print(map_c, ret == exp, ret)
