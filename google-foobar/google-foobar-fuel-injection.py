def solution(n):
    def s_plus_1(s):
        return str(int(s)+1)
    def s_minus_1(s):
        return str(int(s)-1)
        return '',join([s[:-1], str(int(s[-1])-1])])
    def s_div_2(s):
        return str(int(s)//2)


    # Your code here
    if n == '0':
        return 0
    steps = 0
    while True:
        if len(n) == 1:
            steps += {
                '1':0, '2':1, '3':2, '4':2,
                '5':3, '6':3, '7':4, '8':3,
                '9':4}[n]
            return steps

        if n[-1] not in {'0', '2', '4', '6', '8'}:
            d = int(n[-2:])
            if d%4 == 1:
                n = s_minus_1(n)
            else:
                n = s_plus_1(n)
        else:
            n = s_div_2(n)
        steps += 1


cases = [
    ('15', 5),
    ('4', 2),
]

for n, exp in cases:
    ret = solution(n)
    print(n, ret == exp, ret)
