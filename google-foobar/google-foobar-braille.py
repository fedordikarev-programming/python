# bryle_str = """a 	⠁ 	1
# b 	⠃ 	12
# c 	⠉ 	14
# d 	⠙ 	145
# e 	⠑ 	15
# f 	⠋ 	124
# g 	⠛ 	1245
# h 	⠓ 	125
# i 	⠊ 	24
# j 	⠚ 	245
# k 	⠅ 	13
# l 	⠇ 	123
# m 	⠍ 	134
# n 	⠝ 	1345
# o 	⠕ 	135
# p 	⠏ 	1234
# q 	⠟ 	12345
# r 	⠗ 	1235
# s 	⠎ 	234
# t 	⠞ 	2345
# u 	⠥ 	136
# v 	⠧ 	1236
# w 	⠺ 	2456
# x 	⠭ 	1346
# y 	⠽ 	13456
# z 	⠵ 	1356
# """

# for rec in bryle_str.splitlines():
    # letter, _, nums = rec.split()
    # sset = set([int(c) for c in nums])
    # outs = ''.join(['1' if x in sset else '0' for x in range(1,7)])
    # print("{}: '{}',".format(letter, outs))

braille_mapping = {
'a': '100000',
'b': '110000',
'c': '100100',
'd': '100110',
'e': '100010',
'f': '110100',
'g': '110110',
'h': '110010',
'i': '010100',
'j': '010110',
'k': '101000',
'l': '111000',
'm': '101100',
'n': '101110',
'o': '101010',
'p': '111100',
'q': '111110',
'r': '111010',
's': '011100',
't': '011110',
'u': '101001',
'v': '111001',
'w': '010111',
'x': '101101',
'y': '101111',
'z': '101011',    
}

def solution(s):
    # Your code here
    result = list()
    for c in s:
        if c == ' ':
            result.append('000000')
            continue
        if c.upper() == c:
            result.append('000001')
        result.append(braille_mapping[c.lower()])
    return ''.join(result)

cases = [
    ('code', '100100101010100110100010'),
    ('Braille', '000001110000111010100000010100111000111000100010'),
    ('The quick brown fox jumps over the lazy dog', '000001011110110010100010000000111110101001010100100100101000000000110000111010101010010111101110000000110100101010101101000000010110101001101100111100011100000000101010111001100010111010000000011110110010100010000000111000100000101011101111000000100110101010110110'),
]

for test, expt in cases:
    res = solution(test)
    print(test, res == expt, res, expt)
