#  1413. Minimum Value to Get Positive Step by Step Sum
# https://leetcode.com/contest/biweekly-contest-24/problems/minimum-value-to-get-positive-step-by-step-sum/
class Solution:
    def minStartValue(self, nums: List[int]) -> int:
        part_sum = nums[0]
        min_sum = part_sum
        for n in nums[1:]:
            part_sum += n
            if part_sum < min_sum:
                min_sum = part_sum

        if min_sum > 0:
            return 1
        return 1-min_sum

#  1414. Find the Minimum Number of Fibonacci Numbers Whose Sum Is K
# https://leetcode.com/contest/biweekly-contest-24/problems/find-the-minimum-number-of-fibonacci-numbers-whose-sum-is-k/
class Solution:
    def findMinFibonacciNumbers(self, k: int) -> int:
        fn = [1, 1, 2]
        while fn[-1] < k:
            fn.append(fn[-1]+fn[-2])

        count = 0
        n = -1
        while k>0:
            if k >= fn[n]:
                k -= fn[n]
                count += 1
            n -= 1
        return count

#  1415. The k-th Lexicographical String of All Happy Strings of Length n
# https://leetcode.com/contest/biweekly-contest-24/problems/the-k-th-lexicographical-string-of-all-happy-strings-of-length-n/
class Solution:
    def getHappyString(self, n: int, k: int) -> str:
        if k > 3*(2**(n-1)):
            return ""

        res = ["a"]*n
        mul = 2**(n-1)
        res[0] = ["a", "b", "c"][(k-1) // mul]
        k %= mul
        for i in range(1, n):
            mul //= 2
            s = (k-1) // mul
            k %= mul
            res[i] = {'a': 'bc', 'b': 'ac', 'c': 'ab'}[res[i-1]][s]

        return "".join(res)

#  1416. Restore The Array
# https://leetcode.com/contest/biweekly-contest-24/problems/restore-the-array/

