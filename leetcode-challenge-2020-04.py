# 2020.04.01 [ 136] Single Number
# Given a non-empty array of integers, every element appears twice except for one. Find that single one.
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        result = 0
        for n in nums:
            result = result ^ n
        return result


# 2020.04.02 [ 202] Happy Number
# Write an algorithm to determine if a number n is "happy".
class Solution:
    def isHappy(self, n: int) -> bool:
        visited = set()
        visited.add(n)
        while True:
            sum_n = 0
            while n > 0:
                sum_n += (n%10)**2
                n //= 10
            if sum_n == 1:
                return True
            if sum_n in visited:
                return False
            visited.add(sum_n)
            n = sum_n


# 2020.04.03 [ 53 ] Maximum Subarray
# Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

# 2020.04.04 [ 283] Move Zeroes
# Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.
class Soultion:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        dst_index = 0

        for i, n in enumerate(nums):
            if n == 0:
                continue
            nums[dst_index], nums[i] = n, nums[dst_index]
            dst_index += 1
        return

# 2020.04.05 [ 122] Best Time to Buy and Sell Stock II
# Say you have an array prices for which the ith element is the price of a given stock on day i.

# 2020.04.06 [ 49 ] Group Anagrams
# Given an array of strings, group anagrams together.
class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        result = dict()

        for s in strs:
            key = frozenset(s)
            if key not in result:
                result[key] = dict()
            added = False
            for k, v in result[key].items():
                if Counter(k) == Counter(s):
                    v.append(s)
                    added = True
                    break
            if not added:
                result[key][s] = [s]

            # result[key].append(s)

        result_list = list()
        for v in result.values():
            for vv in v.values():
                result_list.append(vv)

        return result_list


# 2020.04.07 [ xxx] Counting Elements
# Given an integer array arr, count element x such that x + 1 is also in arr.
class Solution:
    def countElements(self, arr: List[int]) -> int:
        c = Counter(arr)
        total = 0
        for n, count in c.items():
            total += count if (n+1) in c else 0

        return total

# 2020.04.08 [ 876] Middle of the Linked List
# Given a non-empty, singly linked list with head node head, return a middle node of linked list.
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def middleNode(self, head: ListNode) -> ListNode:
        middle = head
        end = head
        while end and end.next:
            middle = middle.next
            end = end.next.next
            # if end.next:
                # end = end.next
        return middle

# 2020.04.09 [ 844] Backspace String Compare
# Given two strings S and T, return if they are equal when both are typed into empty text editors. # means a backspace character.
class Solution:
    def backspaceCompare(self, S: str, T: str) -> bool:
        def eval_s(s: str) -> str:
            # cur_pos = 0
            res = list()
            for c in s:
                res and res.pop() if c == "#" else res.append(c)

            return res

        return eval_s(S) == eval_s(T)

# 2020.04.10 [ 155] Min Stack
# Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.
class MinStack:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.store = list()
        self.min_store = list()


    def push(self, x: int) -> None:
        self.store.append(x)
        if self.min_store:
            if x <= self.min_store[-1]:
                self.min_store.append(min(x, self.min_store[-1]))
        else:
            self.min_store.append(x)



    def pop(self) -> None:
        if self.min_store and self.store and self.store[-1] == self.min_store[-1]:
            self.min_store.pop()
        if self.store:
            self.store.pop()


    def top(self) -> int:
        return self.store[-1]


    def getMin(self) -> int:
        return self.min_store[-1]

# 2020.04.11 [ 543] Diameter of Binary Tree
# Given a binary tree, you need to compute the length of the diameter of the tree.
class Solution:
    def diameterOfBinaryTree(self, root: TreeNode) -> int:
        def depth_and_length(root: TreeNode) -> (int, int):
            if not root:
                return 0, 0

            r_depth, r_length = 0, 0
            l_depth, l_length = 0, 0
            if root.right:
                r_depth, r_length = depth_and_length(root.right)
            if root.left:
                l_depth, l_length = depth_and_length(root.left)

            return max(l_depth, r_depth)+1, max(l_depth+r_depth, r_length, l_length)

        depth, length = depth_and_length(root)
        return length

# 2020.04.12 [1046] Last Stone Weight
# We have a collection of stones, each stone has a positive integer weight.
class Solution:
    def lastStoneWeight(self, stones: List[int]) -> int:
        stones_c = Counter(stones)
        while True:
            if not stones_c:
                return 0

            max_w = max(stones_c.keys())
            if len(stones_c) == 1:
                if stones_c[max_w] % 2 == 0:
                    return 0
                else:
                    return max_w
            if stones_c[max_w] % 2 == 0:
                del(stones_c[max_w])
                continue
            del(stones_c[max_w])
            second_w = max(stones_c.keys())
            diff_w = max_w - second_w
            stones_c[diff_w] += 1
            if stones_c[second_w] > 1:
                stones_c[second_w] -= 1
            else:
                del(stones_c[second_w])

        return 1

# 2020.04.13 [ 525] Contiguous Array
# Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 1.
class Solution:
    def findMaxLength(self, nums: List[int]) -> int:
        max_len = 0
        summ = 0
        seen_sums = {0: -1}

        for i, n in enumerate(nums):
            summ += 2*n-1
            if summ in seen_sums:
                max_len = max(max_len, i-seen_sums[summ])
            else:
                seen_sums[summ] = i

        return max_len

# 2020.04.14 [ xxx] Perform String Shifts
# You are given a string s containing lowercase English letters, and a matrix shift, where shift[i] = [direction, amount]:
class Solution:
    def stringShift(self, s: str, shift: List[List[int]]) -> str:
        diff = 0
        for direction, count in shift:
            if direction == 1:
                diff += count
            else:
                diff -= count
        diff %= len(s)
        if diff < 0:
            diff += len(s)
        if diff > 0:
            return "".join((s[-diff:], s[:-diff]))
        elif diff < 0:
            return "".join((s[diff:], s[:diff]))
        else:
            return s
